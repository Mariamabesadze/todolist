#pragma warning disable SA1200
using TodoListApp.Services.Database.Entities;
using Microsoft.EntityFrameworkCore;

namespace TodoListApp.Services.Database
{
    public class TaskTagDatabaseService : ITaskTagService
    {
        private readonly TodoListDbContext context;

        public TaskTagDatabaseService(TodoListDbContext context)
        {
            this.context = context;
        }

        public async Task AddTagToTaskAsync(Guid taskId, Guid tagId)
        {
            var existingRelation = await this.context.TaskTags
                .AnyAsync(tt => tt.TaskId == taskId && tt.TagId == tagId);

            if (!existingRelation)
            {
                var taskTag = new TaskTagEntity { TaskId = taskId, TagId = tagId };
                _ = this.context.TaskTags.Add(taskTag);
                _ = await this.context.SaveChangesAsync();
            }
        }

        public async Task<IEnumerable<Models.TaskTag>> GetTagsByTaskAsync(Guid taskId)
        {
            return await this.context.TaskTags
                .Where(tt => tt.TaskId == taskId)
                .Select(tt => new Models.TaskTag
                {
                    TaskId = tt.TaskId,
                    TagId = tt.TagId
                })
                .ToListAsync();
        }

        public async Task<IEnumerable<Models.TaskTag>> GetTasksByTagAsync(Guid tagId)
        {
            return await this.context.TaskTags
                .Where(tt => tt.TagId == tagId)
                .Select(tt => new Models.TaskTag
                {
                    TaskId = tt.TaskId,
                    TagId = tt.TagId
                })
                .ToListAsync();
        }

        public async Task RemoveTagFromTaskAsync(Guid taskId, Guid tagId)
        {
            var taskTag = await this.context.TaskTags
                .FirstOrDefaultAsync(tt => tt.TaskId == taskId && tt.TagId == tagId);

            if (taskTag != null)
            {
                _ = this.context.TaskTags.Remove(taskTag);
                _ = await this.context.SaveChangesAsync();
            }
        }
    }
}
