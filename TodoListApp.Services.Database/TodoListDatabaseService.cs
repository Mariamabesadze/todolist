#pragma warning disable SA1200
using Microsoft.EntityFrameworkCore;
using TodoListApp.Services.Database.Entities;
using TodoListApp.Services.Models;

namespace TodoListApp.Services.Database
{
    public class TodoListDatabaseService : ITodoListService
    {
        private readonly TodoListDbContext context;

        public TodoListDatabaseService(TodoListDbContext context)
        {
            this.context = context;
        }

        public async Task<IEnumerable<TodoList?>> GetAllTodoListsAsync()
        {
            return await this.context.TodoLists
                .Select(t => new TodoList
                {
                    Id = t.Id,
                    Name = t.Name,
                    UserId = t.UserId,
                }).ToListAsync();
        }

        public async Task<TodoList?> GetTodoListByIdAsync(Guid id)
        {
            var entity = await this.context.TodoLists.FindAsync(id);
            if (entity == null)
            {
                return null;
            }

            return new TodoList
            {
                Id = entity.Id,
                Name = entity.Name,
                UserId = entity.UserId,
            };
        }

        public async Task<TodoList> CreateTodoListAsync(TodoList todoList)
        {
            var entity = new TodoListEntity
            {
                Id = Guid.NewGuid(),
                Name = todoList.Name,
                UserId = todoList.UserId,
            };

            _ = this.context.TodoLists.Add(entity);
            _ = await this.context.SaveChangesAsync();

            todoList.Id = entity.Id;
            return todoList;
        }

        public async Task UpdateTodoListAsync(TodoList todoList)
        {
            var entity = await this.context.TodoLists.FindAsync(todoList.Id) ?? throw new Exception("TodoList not found");
            entity.Name = todoList.Name;

            this.context.Entry(entity).State = EntityState.Modified;
            _ = await this.context.SaveChangesAsync();
        }

        public async Task DeleteTodoListAsync(Guid id)
        {
            var entity = await this.context.TodoLists.FindAsync(id);
            if (entity != null)
            {
                _ = this.context.TodoLists.Remove(entity);
                _ = await this.context.SaveChangesAsync();
            }
        }
    }
}
