namespace TodoListApp.Services.Database.Entities
{
    public class TodoTaskEntity
    {
        public Guid Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public DateTime DueDate { get; set; }

        public bool IsCompleted { get; set; }

        public Guid TodoListId { get; set; }

        public TodoListEntity TodoList { get; set; }

        public List<TaskTagEntity> TaskTags { get; set; }
    }
}
