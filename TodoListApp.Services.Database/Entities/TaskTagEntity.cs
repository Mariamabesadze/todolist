namespace TodoListApp.Services.Database.Entities
{
    public class TaskTagEntity
    {
        public Guid TaskId { get; set; }
        public TodoTaskEntity Task { get; set; }

        public Guid TagId { get; set; }
        public TagEntity Tag { get; set; }
    }
}
