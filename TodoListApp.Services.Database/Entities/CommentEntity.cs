﻿namespace TodoListApp.Services.Database.Entities
{
    public class CommentEntity
    {

        public Guid Id { get; set; }

        public string Content { get; set; }

        public DateTime CreatedAt { get; set; } = DateTime.UtcNow;

        public Guid TaskId { get; set; } // Foreign key to the TaskEntity

        public TodoTaskEntity Task { get; set; } // Navigation property
    }
}
