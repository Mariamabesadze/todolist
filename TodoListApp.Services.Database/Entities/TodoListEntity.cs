#pragma warning disable SA1200
using System.ComponentModel.DataAnnotations;

namespace TodoListApp.Services.Database.Entities
{
    public class TodoListEntity
    {
        [Key]
        public Guid Id { get; set; }

        public string? Name { get; set; }

        public Guid? UserId { get; set; }
    }
}
