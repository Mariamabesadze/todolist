﻿namespace TodoListApp.Services.Database.Entities
{
    public class TagEntity
    {
        public Guid Id { get; set; }

        public string Title { get; set; }

        public List<TaskTagEntity> TaskTags { get; set; }
    }
}