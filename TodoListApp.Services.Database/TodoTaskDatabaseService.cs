#pragma warning disable SA1200

using Microsoft.EntityFrameworkCore;
using TodoListApp.Services.Database.Entities;
using TodoListApp.Services.Models;

namespace TodoListApp.Services.Database
{
    public class TodoTaskDatabaseService : ITodoTaskService
    {
        private readonly TodoListDbContext context;

        public TodoTaskDatabaseService(TodoListDbContext context)
        {
            this.context = context;
        }

        public async Task<IEnumerable<TodoTask>> GetTasksByListIdAsync(Guid todoListId)
        {
            return await this.context.TodoTasks
                .Where(t => t.TodoListId == todoListId)
                .Select(t => new TodoTask
                {
                    Id = t.Id,
                    Title = t.Title,
                    Description = t.Description,
                    DueDate = t.DueDate,
                    IsCompleted = t.IsCompleted,
                    TodoListId = t.TodoListId,
                }).ToListAsync();
        }

        public async Task<TodoTask?> GetTaskByIdAsync(Guid taskId)
        {
            var entity = await this.context.TodoTasks.FindAsync(taskId);
            if (entity == null)
            {
                return null;
            }

            return new TodoTask
            {
                Id = entity.Id,
                Title = entity.Title,
                Description = entity.Description,
                DueDate = entity.DueDate,
                IsCompleted = entity.IsCompleted,
                TodoListId = entity.TodoListId,
            };
        }

        public async Task<TodoTask> CreateTaskAsync(TodoTask todoTask)
        {
            var entity = new TodoTaskEntity
            {
                Id = Guid.NewGuid(),
                Title = todoTask.Title,
                Description = todoTask.Description,
                DueDate = todoTask.DueDate,
                IsCompleted = todoTask.IsCompleted,
                TodoListId = todoTask.TodoListId,
            };

            _ = this.context.TodoTasks.Add(entity);
            _ = await this.context.SaveChangesAsync();

            todoTask.Id = entity.Id; // Ensure the returned model has the DB-generated ID
            return todoTask;
        }

        public async Task UpdateTaskAsync(TodoTask todoTask)
        {
            var entity = await this.context.TodoTasks.FindAsync(todoTask.Id) ?? throw new Exception("TodoTask not found");
            entity.Title = todoTask?.Title;
            entity.Description = todoTask.Description;
            entity.DueDate = todoTask.DueDate;
            entity.IsCompleted = todoTask.IsCompleted;

            this.context.Entry(entity).State = EntityState.Modified;
            _ = await this.context.SaveChangesAsync();
        }

        public async Task DeleteTaskAsync(Guid id)
        {
            var entity = await this.context.TodoTasks.FindAsync(id);
            if (entity != null)
            {
                _ = this.context.TodoTasks.Remove(entity);
                _ = await this.context.SaveChangesAsync();
            }
        }
    }
}
