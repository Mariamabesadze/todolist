#pragma warning disable SA1200

using Microsoft.EntityFrameworkCore;
using TodoListApp.Services.Database.Entities;
using TodoListApp.Services.Models;

namespace TodoListApp.Services.Database
{
    public class TagDatabaseService : ITagService
    {
        private readonly TodoListDbContext _context;

        public TagDatabaseService(TodoListDbContext context)
        {
            this._context = context;
        }

        public async Task UpdateTagAsync(Tag tag)
        {
            var entity = await this._context.Tags.FindAsync(tag.Id);
            if (entity == null)
            {
                throw new Exception("TodoTask not found");
            }

            entity.Title = tag.Title;

            this._context.Entry(entity).State = EntityState.Modified;
            _ = await this._context.SaveChangesAsync();
        }

        public async Task DeleteTagAsync(Guid id)
        {
            var entity = await this._context.Tags.FindAsync(id);
            if (entity != null)
            {
                _ = this._context.Tags.Remove(entity);
                _ = await this._context.SaveChangesAsync();
            }
        }

        public async Task<IEnumerable<Tag>> GetAllTagsAsync()
        {
            return await this._context.Tags
                          .Select(t => new Tag
                          {
                              Id = t.Id,
                              Title = t.Title
                          }).ToListAsync();
        }

        public async Task<Tag> GetTagByIdAsync(Guid id)
        {
            var tagEntity = await this._context.Tags.FindAsync(id);
            if (tagEntity == null)
            {
                return null; // or throw an exception based on your preference
            }

            return new Tag
            {
                Id = tagEntity.Id,
                Title = tagEntity.Title
                // Map other properties if there are any
            };
        }

        public async Task<Tag> CreateTagAsync(Tag tag)
        {
            var tagEntity = new TagEntity
            {
                Id = Guid.NewGuid(), // or let the database generate it
                Title = tag.Title
                // Map other properties if there are any
            };

            _ = this._context.Tags.Add(tagEntity);
            _ = await this._context.SaveChangesAsync();

            tag.Id = tagEntity.Id; // Ensure the returned model has the DB-generated ID
            return tag;
        }

    }
}
