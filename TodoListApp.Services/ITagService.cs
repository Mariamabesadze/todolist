#pragma warning disable SA1200

using TodoListApp.Services.Models;

namespace TodoListApp.Services
{
    public interface ITagService
    {
        Task<IEnumerable<Tag>> GetAllTagsAsync();

        Task<Tag> GetTagByIdAsync(Guid id);

        Task<Tag> CreateTagAsync(Tag tag);

        Task DeleteTagAsync(Guid id);

        Task UpdateTagAsync(Tag tag);
    }
}
