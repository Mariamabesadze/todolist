#pragma warning disable SA1200

using TodoListApp.Services.Models;

namespace TodoListApp.Services
{
    public interface ICommentService
    {
        Task<IEnumerable<Comment>> GetCommentsByTaskIdAsync(Guid taskId);

        Task<Comment> AddCommentAsync(Comment comment);

        Task DeleteCommentAsync(Guid commentId);

        Task<Comment> EditCommentAsync(Guid commentId, string newContent);
    }
}
