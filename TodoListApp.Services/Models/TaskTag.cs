namespace TodoListApp.Services.Models
{
    public class TaskTag
    {
        public Guid TaskId { get; set; }

        public Guid TagId { get; set; }
    }
}
