namespace TodoListApp.Services.Models
{
    public class TodoList
    {
        public Guid Id { get; set; }

        public string? Name { get; set; }

        public Guid? UserId { get; set; }

        public IEnumerable<TodoTask>? Tasks
        {
            get; set;
        }
    }
}
