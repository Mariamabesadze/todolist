#pragma warning disable SA1200

using TodoListApp.Services.Models;

namespace TodoListApp.Services
{
    public interface ITodoTaskService
    {
        Task<IEnumerable<TodoTask>> GetTasksByListIdAsync(Guid todoListId);

        Task<TodoTask?> GetTaskByIdAsync(Guid taskId);

        Task<TodoTask> CreateTaskAsync(TodoTask todoTask);

        Task UpdateTaskAsync(TodoTask todoTask);

        Task DeleteTaskAsync(Guid id);
    }
}
