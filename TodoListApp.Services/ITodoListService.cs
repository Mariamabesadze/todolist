#pragma warning disable SA1200

using TodoListApp.Services.Models;

namespace TodoListApp.Services
{
    public interface ITodoListService
    {
        Task<IEnumerable<TodoList?>> GetAllTodoListsAsync();

        Task<TodoList?> GetTodoListByIdAsync(Guid id);

        Task<TodoList> CreateTodoListAsync(TodoList todoList);

        Task UpdateTodoListAsync(TodoList todoList);

        Task DeleteTodoListAsync(Guid id);
    }
}
