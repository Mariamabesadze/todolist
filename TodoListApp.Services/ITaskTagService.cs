#pragma warning disable SA1200

using TodoListApp.Services.Models;

namespace TodoListApp.Services
{
    public interface ITaskTagService
    {
        Task<IEnumerable<TaskTag>> GetTasksByTagAsync(Guid tagId);

        Task<IEnumerable<TaskTag>> GetTagsByTaskAsync(Guid taskId);

        Task AddTagToTaskAsync(Guid taskId, Guid tagId);

        Task RemoveTagFromTaskAsync(Guid taskId, Guid tagId);
    }
}
