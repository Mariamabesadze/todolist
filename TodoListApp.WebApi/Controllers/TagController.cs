#pragma warning disable SA1200

using Microsoft.AspNetCore.Mvc;
using TodoListApp.Services;
using TodoListApp.Services.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace TodoListApp.WebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TagController : ControllerBase
    {
        private readonly ITagService _tagService;

        public TagController(ITagService tagService)
        {
            _tagService = tagService;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Tag>>> GetAllTags()
        {
            var tags = await _tagService.GetAllTagsAsync();
            return Ok(tags);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Tag>> GetTag(Guid id)
        {
            var tag = await _tagService.GetTagByIdAsync(id);
            if (tag == null)
            {
                return NotFound($"Tag with ID {id} not found");
            }
            return Ok(tag);
        }

        [HttpPost]
        public async Task<ActionResult<Tag>> CreateTag([FromBody] Tag tag)
        {
            var createdTag = await _tagService.CreateTagAsync(tag);
            return CreatedAtAction(nameof(GetTag), new { id = createdTag.Id }, createdTag);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateTag(Guid id, [FromBody] Tag tag)
        {
            if (id != tag.Id)
            {
                return BadRequest("ID in the URL does not match the ID in the tag object");
            }

            try
            {
                await _tagService.UpdateTagAsync(tag);
                return NoContent();
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Tag not found"))
                {
                    return NotFound(ex.Message);
                }
                return StatusCode(500, ex.Message);
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTag(Guid id)
        {
            try
            {
                await _tagService.DeleteTagAsync(id);
                return NoContent();
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Tag not found"))
                {
                    return NotFound(ex.Message);
                }

                return StatusCode(500, "An error occurred while deleting the tag");
            }
        }
    }
}
