#pragma warning disable SA1200

using Microsoft.AspNetCore.Mvc;
using TodoListApp.Services;

namespace TodoListApp.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TaskTagController : ControllerBase
    {
        private readonly ITaskTagService _taskTagService;

        public TaskTagController(ITaskTagService taskTagService)
        {
            _taskTagService = taskTagService;
        }

        // GET: api/TaskTag/{tagId}
        [HttpGet("tasks/{tagId}")]
        public async Task<IActionResult> GetTasksByTag(Guid tagId)
        {
            var tasks = await _taskTagService.GetTasksByTagAsync(tagId);
            return Ok(tasks);
        }
        [HttpGet("tags/{tagId}")]
        public async Task<IActionResult> GetTagsByTask(Guid tasksId)
        {
            var tasks = await _taskTagService.GetTagsByTaskAsync(tasksId);
            return Ok(tasks);
        }

        // POST: api/TaskTag/{taskId}/{tagId}
        [HttpPost("{taskId}/{tagId}")]
        public async Task<IActionResult> AddTagToTask(Guid taskId, Guid tagId)
        {
            await _taskTagService.AddTagToTaskAsync(taskId, tagId);
            return NoContent(); // 204 No Content is typically used to indicate success without returning data
        }

        // DELETE: api/TaskTag/{taskId}/{tagId}
        [HttpDelete("{taskId}/{tagId}")]
        public async Task<IActionResult> RemoveTagFromTask(Guid taskId, Guid tagId)
        {
            await _taskTagService.RemoveTagFromTaskAsync(taskId, tagId);
            return NoContent();
        }
    }
}

