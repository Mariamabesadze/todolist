#pragma warning disable SA1200

using Microsoft.AspNetCore.Mvc;
using TodoListApp.Services;
using TodoListApp.Services.Models;

namespace TodoListApp.WebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TodoListController : ControllerBase
    {
        private readonly ITodoListService todoListService;

        public TodoListController(ITodoListService todoListService)
        {
            this.todoListService = todoListService;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<TodoList>>> GetAllTodoLists()
        {
            var todoLists = await this.todoListService.GetAllTodoListsAsync();
            return this.Ok(todoLists);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<TodoList>> GetTodoList(Guid id)
        {
            var todoList = await this.todoListService.GetTodoListByIdAsync(id);
            if (todoList == null)
            {
                return this.NotFound();
            }

            return todoList;
        }

        [HttpPost]
        public async Task<ActionResult<TodoList>> CreateTodoList([FromBody] TodoList todoList)
        {
            var createdTodoList = await this.todoListService.CreateTodoListAsync(todoList);
            return this.CreatedAtAction(nameof(this.GetTodoList), new { id = createdTodoList.Id }, createdTodoList);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateTodoList(Guid id, [FromBody] TodoList todoList)
        {
            if (id != todoList.Id)
            {
                return this.BadRequest();
            }

            await this.todoListService.UpdateTodoListAsync(todoList);
            return this.NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTodoList(Guid id)
        {
            await this.todoListService.DeleteTodoListAsync(id);
            return this.NoContent();
        }
    }
}
