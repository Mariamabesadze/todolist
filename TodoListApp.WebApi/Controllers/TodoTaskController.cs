#pragma warning disable SA1200

using Microsoft.AspNetCore.Mvc;
using TodoListApp.Services;
using TodoListApp.Services.Models;

namespace TodoListApp.WebApi.Controllers
{
    [ApiController]
    [Route("api/todolists/{todoListId}/tasks")]
    public class TodoTaskController : ControllerBase
    {
        private readonly ITodoTaskService todoTaskService;

        public TodoTaskController(ITodoTaskService todoTaskService)
        {
            this.todoTaskService = todoTaskService;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<TodoTask>>> GetTasks(Guid todoListId)
        {
            var tasks = await this.todoTaskService.GetTasksByListIdAsync(todoListId);
            return this.Ok(tasks);
        }

        [HttpGet("{taskId}")]
        public async Task<ActionResult<TodoTask>> GetTask(Guid taskId)
        {
            var task = await this.todoTaskService.GetTaskByIdAsync(taskId);
            if (task == null)
            {
                return this.NotFound();
            }

            return task;
        }

        [HttpPost]
        public async Task<ActionResult<TodoTask>> CreateTask(Guid todoListId, [FromBody] TodoTask task)
        {
            if (task == null)
            {
                return this.BadRequest("Task cannot be null");
            }

            task.TodoListId = todoListId; // Ensure the task is associated with the correct todo list
            var createdTask = await this.todoTaskService.CreateTaskAsync(task);
            return this.CreatedAtAction(nameof(this.GetTask), new { todoListId = createdTask.TodoListId, taskId = createdTask.Id }, createdTask);
        }

        [HttpPut("{taskId}")]
        public async Task<IActionResult> UpdateTask(Guid todoListId, Guid taskId, [FromBody] TodoTask task)
        {
            if (task == null || taskId != task.Id)
            {
                return this.BadRequest();
            }

            task.TodoListId = todoListId; // Ensure the task is associated with the correct todo list
            await this.todoTaskService.UpdateTaskAsync(task);
            return this.NoContent();
        }

        [HttpDelete("{taskId}")]
        public async Task<IActionResult> DeleteTask(Guid taskId)
        {
            await this.todoTaskService.DeleteTaskAsync(taskId);
            return this.NoContent();
        }
    }
}
