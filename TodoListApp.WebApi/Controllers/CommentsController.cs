#pragma warning disable SA1200

using Microsoft.AspNetCore.Mvc;
using TodoListApp.Services.Models;
using TodoListApp.Services;

namespace TodoListApp.WebApi.Controllers
{
    public class CommentsController : Controller
    {
        private readonly ICommentService _commentService;

        public CommentsController(ICommentService commentService)
        {
            _commentService = commentService;
        }

        [HttpGet]
        public async Task<IActionResult> GetComments(Guid taskId)
        {
            var comments = await _commentService.GetCommentsByTaskIdAsync(taskId);
            return Ok(comments);
        }

        [HttpPost]
        public async Task<IActionResult> AddComment(Guid taskId, [FromBody] Comment comment)
        {
            comment.TaskId = taskId;
            var addedComment = await _commentService.AddCommentAsync(comment);
            return CreatedAtAction(nameof(GetComments), new { taskId = addedComment.TaskId }, addedComment);
        }

        [HttpDelete("{commentId}")]
        public async Task<IActionResult> DeleteComment(Guid commentId)
        {
            await _commentService.DeleteCommentAsync(commentId);
            return NoContent();
        }

        [HttpPut("{commentId}")]
        public async Task<IActionResult> EditComment(Guid commentId, [FromBody] string newContent)
        {
            var updatedComment = await _commentService.EditCommentAsync(commentId, newContent);
            if (updatedComment == null)
            {
                return NotFound();
            }
            return Ok(updatedComment);
        }
    }
}
