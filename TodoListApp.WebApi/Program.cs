#pragma warning disable SA1200

using Microsoft.EntityFrameworkCore;
using TodoListApp.Services.Database;
using TodoListApp.Services;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddDbContext<TodoListDbContext>(options =>
    options.UseSqlServer(builder.Configuration.GetConnectionString("TodoListDbContext")));
builder.Services.AddScoped<ITodoListService, TodoListDatabaseService>();
builder.Services.AddScoped<ITodoTaskService, TodoTaskDatabaseService>(); // Add this line
builder.Services.AddScoped<ITagService, TagDatabaseService>();
builder.Services.AddScoped<ITaskTagService, TaskTagDatabaseService>();
builder.Services.AddScoped<ICommentService, CommentDatabaseService>();

builder.Services.AddControllers();

var app = builder.Build();

if (!app.Environment.IsDevelopment())
{
    _ = app.UseExceptionHandler("/Error");
}

app.UseHttpsRedirection();
app.UseAuthorization();
app.MapControllers();

app.Run();
