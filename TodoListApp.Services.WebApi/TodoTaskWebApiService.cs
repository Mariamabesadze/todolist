#pragma warning disable SA1200

using System.Net.Http.Json;
using TodoListApp.Services.Models;

public class TodoTaskWebApiService
{
    private readonly HttpClient _httpClient;

    public TodoTaskWebApiService(HttpClient httpClient)
    {
        this._httpClient = httpClient;
    }

    public async Task<IEnumerable<TodoTask>> GetTasksAsync(Guid todoListId)
    {
        return await this._httpClient.GetFromJsonAsync<IEnumerable<TodoTask>>($"api/todolists/{todoListId}/tasks");
    }

    public async Task<TodoTask> GetTaskAsync(Guid todoListId, Guid taskId)
    {
        return await this._httpClient.GetFromJsonAsync<TodoTask>($"api/todolists/{todoListId}/tasks/{taskId}");
    }

    public async Task AddTaskAsync(Guid todoListId, TodoTask task)
    {
        var response = await this._httpClient.PostAsJsonAsync($"api/todolists/{todoListId}/tasks", task);
        response.EnsureSuccessStatusCode();
    }

    public async Task DeleteTaskAsync(Guid todoListId, Guid taskId)
    {
        var response = await this._httpClient.DeleteAsync($"api/todolists/{todoListId}/tasks/{taskId}");
        response.EnsureSuccessStatusCode();
    }

    public async Task UpdateTaskAsync(Guid todoListId, TodoTask task)
    {
        var response = await this._httpClient.PutAsJsonAsync($"api/todolists/{todoListId}/tasks/{task.Id}", task);
        response.EnsureSuccessStatusCode();
    }
}
