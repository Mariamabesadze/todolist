#pragma warning disable SA1200

using System.Net.Http.Json;
using TodoListApp.Services.Models;

namespace TodoListApp.Services.WebApi
{
    public class TodoListWebApiService
    {
        private readonly HttpClient httpClient;

        public TodoListWebApiService(HttpClient httpClient)
        {
            this.httpClient = httpClient;
        }

        public async Task<IEnumerable<TodoList>> GetTodoListsAsync()
        {
            return await this.httpClient.GetFromJsonAsync<IEnumerable<TodoList>>("api/TodoList");
        }

        public async Task<TodoList> GetTodoListAsync(Guid id)
        {
            return await this.httpClient.GetFromJsonAsync<TodoList>($"api/TodoList/{id}");
        }

        public async Task<TodoList> AddTodoListAsync(TodoList todoList)
        {
            var response = await this.httpClient.PostAsJsonAsync("api/TodoList", todoList);

            return await response.Content.ReadFromJsonAsync<TodoList>();
        }

        public async Task DeleteTodoListAsync(Guid id)
        {
            var response = await this.httpClient.DeleteAsync($"api/TodoList/{id}");
            response.EnsureSuccessStatusCode();
        }

        public async Task UpdateTodoListAsync(TodoList todoList)
        {
            var response = await this.httpClient.PutAsJsonAsync($"api/TodoList/{todoList.Id}", todoList);
            response.EnsureSuccessStatusCode();
        }
    }
}
