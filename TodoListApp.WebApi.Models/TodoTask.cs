namespace TodoListApp.WebApi.Models
{
    public class TodoTask
    {
        public Guid Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public DateTime DueDate { get; set; }

        public bool IsCompleted { get; set; }

        public Guid TodoListId { get; set; }
    }
}
