#pragma warning disable SA1200

using TodoListApp.Services.WebApi;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using TodoListApp.WebApp;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddHttpClient<TodoListWebApiService>(client =>
{
    client.BaseAddress = new Uri("https://localhost:7182/");
});

builder.Services.AddHttpClient<TodoTaskWebApiService>(client =>
{
    client.BaseAddress = new Uri(builder.Configuration["ApiBaseUrl"]); // Assuming ApiBaseUrl is defined in appsettings.json
});

builder.Services.AddDbContext<UserDbContext>(options =>
    options.UseSqlServer(builder.Configuration.GetConnectionString("UserDbContext")));

// Configure ASP.NET Core Identity
builder.Services.AddDefaultIdentity<IdentityUser>(options => options.SignIn.RequireConfirmedAccount = true)
    .AddRoles<IdentityRole>() // Add this line if you need role-based functionality
    .AddEntityFrameworkStores<UserDbContext>()
    .AddDefaultTokenProviders();

// Add services for MVC and Razor Pages
builder.Services.AddControllersWithViews();
builder.Services.AddRazorPages(); // Add this line to include Razor Pages

var app = builder.Build();

if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthentication(); // Enable authentication
app.UseAuthorization();

// Logging middleware for debugging purposes
app.Use(async (context, next) =>
{
    Console.WriteLine($"Incoming request path: {context.Request.Path}");
    await next();
});

// Map controllers and Razor Pages in the endpoint routing
app.MapControllerRoute(
    name: "default",
    pattern: "{controller=TodoList}/{action=Index}/{id?}");
app.MapRazorPages(); // Map Razor Pages

app.Run();
