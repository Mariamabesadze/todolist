#pragma warning disable SA1200

using Microsoft.AspNetCore.Mvc;

using TodoListApp.Services.Models; // Ensure this matches the namespace of your TodoTask model

namespace TodoListApp.WebApp.Controllers
{
    public class TodoTaskController : Controller
    {
        private readonly TodoTaskWebApiService _todoTaskService;

        public TodoTaskController(TodoTaskWebApiService todoTaskService)
        {
            _todoTaskService = todoTaskService;
        }

        public async Task<IActionResult> Index(Guid todoListId)
        {
            var tasks = await _todoTaskService.GetTasksAsync(todoListId);
            return View(tasks); // Assumes you have an Index view for listing tasks
        }

        public async Task<IActionResult> Details(Guid todoListId, Guid taskId)
        {
            try
            {
                var task = await _todoTaskService.GetTaskAsync(todoListId, taskId);
                if (task == null)
                {
                    // Log the event or handle it as needed
                    return NotFound($"Task with ID {taskId} in TodoList with ID {todoListId} was not found.");
                }

                return View(task); // Passes the task to the Details view
            }
            catch (HttpRequestException ex) when (ex.StatusCode == System.Net.HttpStatusCode.NotFound)
            {
                // Log the error or handle it as needed
                return NotFound($"Task with ID {taskId} in TodoList with ID {todoListId} was not found.");
            }
            catch (Exception ex)
            {
                // Log the error or handle it as needed
                return StatusCode(500, "An error occurred while processing your request.");
            }
        }

        public IActionResult Create(Guid todoListId)
        {
            return View(new TodoTask { TodoListId = todoListId }); // Pass a new task model to your Create view
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Guid todoListId, TodoTask task)
        {

            if (ModelState.IsValid)
            {

                await _todoTaskService.AddTaskAsync(todoListId, task);

                return RedirectToAction(nameof(Index), new { todoListId });
            }
            return View(task);
        }

        // GET: TodoTask/Delete/5
        public async Task<IActionResult> Delete(Guid? todoListId, Guid? taskId)
        {
            if (todoListId == null || taskId == null)
            {
                return NotFound();
            }

            var task = await _todoTaskService.GetTaskAsync(todoListId.Value, taskId.Value);
            if (task == null)
            {
                return NotFound();
            }

            return View(task); // Show a view that asks the user to confirm the deletion
        }

        [HttpPost, ActionName("DeleteConfirmed")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid todoListId, Guid taskId)
        {
            await _todoTaskService.DeleteTaskAsync(todoListId, taskId);
            return Redirect(Request.Headers["Referer"].ToString());
        }


        // GET: TodoTask/Edit/5
        public async Task<IActionResult> Edit(Guid todoListId, Guid taskId)
        {
            try
            {
                var task = await _todoTaskService.GetTaskAsync(todoListId, taskId);
                if (task == null)
                {
                    return NotFound($"Task with ID {taskId} in TodoList with ID {todoListId} was not found.");
                }

                return View(task); // Passes the task to the Edit view
            }
            catch (HttpRequestException ex) when (ex.StatusCode == System.Net.HttpStatusCode.NotFound)
            {
                // Log the error or handle it as needed
                return NotFound($"Task with ID {taskId} in TodoList with ID {todoListId} was not found.");
            }
            catch (Exception ex)
            {
                // Log the error or handle it as needed
                return StatusCode(500, "An error occurred while processing your request.");
            }
        }

        // POST: TodoTask/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid todoListId, TodoTask task)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    await _todoTaskService.UpdateTaskAsync(todoListId, task);
                    return RedirectToAction(nameof(Details), new { todoListId, taskId = task.Id });
                }
                catch (HttpRequestException ex)
                {
                    // Log the error or handle it as needed
                    ModelState.AddModelError("", "Error occurred while trying to update the task.");
                }
                catch (Exception ex)
                {
                    // Log the error or handle it as needed
                    return StatusCode(500, "An error occurred while processing your request.");
                }
            }

            return View(task);
        }
    }
}
