#pragma warning disable SA1200

using Microsoft.AspNetCore.Mvc;
using TodoListApp.Services.WebApi;
using Microsoft.AspNetCore.Identity;

namespace TodoListApp.WebApp.Controllers
{
    public class TodoListController : Controller
    {
        private readonly TodoListWebApiService _todoListService;
        private readonly TodoTaskWebApiService _todoTaskService;
        private readonly UserManager<IdentityUser> _userManager;

        public TodoListController(TodoListWebApiService todoListService, TodoTaskWebApiService todoTaskService, UserManager<IdentityUser> userManager)
        {
            _todoListService = todoListService;
            _todoTaskService = todoTaskService;
            _userManager = userManager;

        }

        public async Task<IActionResult> Index()
        {
            var todoLists = await _todoListService.GetTodoListsAsync();
            var viewModelLists = todoLists.Select(t => new TodoListApp.Services.Models.TodoList
            {
                Id = t.Id,
                Name = t.Name,
                UserId = t.UserId

                // Map other properties as needed
            });
            var returnList = new List<TodoListApp.Services.Models.TodoList>();
            foreach (var list in viewModelLists)
            {
                var newObject = new TodoListApp.Services.Models.TodoList
                {
                    Id = list.Id,
                    Name = list.Name,
                    UserId = list.UserId,
                    // Add additional properties here
                    Tasks = await _todoTaskService.GetTasksAsync(list.Id),
                };

                returnList.Add(newObject);
            }

            return View(returnList);
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(TodoListApp.Services.Models.TodoList todoList)
        {
            var user = await _userManager.GetUserAsync(User);
            if (user != null)
            {
                if (ModelState.IsValid)
                {
                    Guid userIdGuid;

                    if (Guid.TryParse(user.Id, out userIdGuid))
                    {
                        todoList.UserId = userIdGuid;
                    }
                    await _todoListService.AddTodoListAsync(todoList);
                    return RedirectToAction(nameof(Index));
                }
            }
            if (ModelState.IsValid)
            {
                await _todoListService.AddTodoListAsync(todoList);
                return RedirectToAction(nameof(Index));
            }
            return View(todoList);
        }

        [HttpPost, ActionName("Delete")]
        public async Task<IActionResult> Delete(Guid todoListId)
        {
            await _todoListService.DeleteTodoListAsync(todoListId);
            return Redirect(Request.Headers["Referer"].ToString());
        }

        [HttpGet]
        public async Task<IActionResult> Edit(Guid id)
        {
            var todoList = await _todoListService.GetTodoListAsync(id);
            if (todoList == null)
            {
                return NotFound();
            }

            return View(todoList);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(TodoListApp.Services.Models.TodoList todoList)
        {
            await _todoListService.UpdateTodoListAsync(todoList);
            return RedirectToAction(nameof(Index));
        }


    }
}
